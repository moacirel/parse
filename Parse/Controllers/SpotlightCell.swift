//
//  SpotlightCell.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public class SpotlightCell: UICollectionViewCell {
    
    var data: SpotlightVO? {
        didSet {
            guard let data = data else { return }
            DownloadImage.getImageFor(urlString: data.bannerURL ?? "", completion: { (image, error) in
                if let image = image {
                    DispatchQueue.main.async {
                        self.bg.image = image
                    }
                }
            })
        }
    }
    
    fileprivate let bg: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 12

        return iv
    }()

    public override func layoutSubviews() {
        let radius: CGFloat = 12
        contentView.layer.cornerRadius = radius
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true

        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1.0)
        layer.shadowRadius = 0.5
        layer.shadowOpacity = 0.1
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: radius).cgPath
        layer.cornerRadius = radius
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        createView()
    }
    
    private func createView() {
        contentView.addSubview(bg)

        setupConstraints()
    }

    private func setupConstraints() {
            NSLayoutConstraint.activate([
                bg.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 1),
                bg.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 1),
                bg.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -1),
                bg.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -1),
            ])
        }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
