//
//  ViewController.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    fileprivate var dataSource = AmazonAwsVO()
    
    fileprivate let viewError = ErrorView(frame: .zero)
    fileprivate let viewCash = CashView(frame: .zero)
    fileprivate let viewProduct = ProductView(frame: .zero)
    fileprivate let viewSpotlight = SpotlightView(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = BackGroundColorDefault
        
        requestAmazonAws()
    }
    
    func requestAmazonAws() {
        AmazonAwsService.getData { (aws, error) in
            if let error = error {
                debugPrint(error)
                DispatchQueue.main.async {
                    self.createViewError()
                }
            } else
            if let aws = aws {
                DispatchQueue.main.async {
                    self.dataSource = AmazonAwsParser().parseToVo(amazonAws: aws)
                    
                    self.viewSpotlight.data = self.dataSource.spotlightVO
                    self.viewSpotlight.collectionViewSpotlight.reloadData()
                    
                    self.viewCash.data = self.dataSource.cashVO
                    
                    self.viewProduct.data = self.dataSource.productsVO
                    self.viewProduct.collectionViewProduct.reloadData()
                    
                    self.createView()
                }
            }
        }
    }
    
    private func createView() {
            viewSpotlight.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(viewSpotlight)
            
            viewCash.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(viewCash)
            
            viewProduct.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(viewProduct)
            
            setupConstraints()
    }
    
    private func createViewError() {
        viewError.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(viewError)
        
        viewError.button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        viewError.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        viewError.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0).isActive = true
        viewError.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0).isActive = true
        viewError.heightAnchor.constraint(equalTo: viewError.widthAnchor, multiplier: 0.5).isActive = true
    }
    
    @objc func buttonAction(sender: UIButton!) {
       requestAmazonAws()
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            viewSpotlight.topAnchor.constraint(equalTo: view.topAnchor, constant: 60),
            viewSpotlight.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0),
            viewSpotlight.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8.0),
            viewSpotlight.heightAnchor.constraint(equalTo: viewSpotlight.widthAnchor, multiplier: 0.7),
            
            viewCash.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            viewCash.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0),
            viewCash.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0),
            viewCash.heightAnchor.constraint(equalTo: viewCash.widthAnchor, multiplier: 0.5),
            
            viewProduct.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50),
            viewProduct.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16.0),
            viewProduct.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16.0),
            viewProduct.heightAnchor.constraint(equalTo: viewProduct.widthAnchor, multiplier: 0.6),
        ])
    }    
}


