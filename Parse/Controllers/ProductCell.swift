//
//  ProductCell.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public class ProductCell: UICollectionViewCell {
    
    var data: ProductVO? {
        didSet {
            guard let data = data else { return }
            DownloadImage.getImageFor(urlString: data.imageURL ?? "", completion: { (image, error) in
                if let image = image {
                    DispatchQueue.main.async {
                        self.bg.image = image
                        self.titleLabel.text = ""
                        
                        if (error != nil)
                        {
                            self.titleLabel.text = data.name
                        }
                    }
                }
            })
        }
    }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.textColor = .black
        label.textAlignment = .center
        
        return label
    }()
    
    fileprivate let viewBackground: UIView = {
        let v = UIView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .white
        v.layer.cornerRadius = 12
        
        return v
    }()
    
    fileprivate let bg: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 12

        return iv
    }()

    public override func layoutSubviews() {
        let radius: CGFloat = 12
        contentView.layer.cornerRadius = radius
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true

        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1.0)
        layer.shadowRadius = 0.5
        layer.shadowOpacity = 0.1
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: radius).cgPath
        layer.cornerRadius = radius
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        createView()
    }
    
    private func createView() {
        viewBackground.addSubview(titleLabel)
        viewBackground.addSubview(bg)
        contentView.addSubview(viewBackground)
        
        setupConstraints()
    }

    private func setupConstraints() {
            NSLayoutConstraint.activate([
                viewBackground.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 1),
                viewBackground.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 1),
                viewBackground.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -0.5),
                viewBackground.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -0.5),
                
                titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                titleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 4),
                titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -4),
                titleLabel.heightAnchor.constraint(equalToConstant: 26),

                bg.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                bg.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                bg.widthAnchor.constraint(equalToConstant: 70),
                bg.heightAnchor.constraint(equalToConstant: 70),
            ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

