//
//  CashView.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public class CashView: UIView {
    
    var data: CashVO? {
        didSet {
            guard let data = data else { return }
            DownloadImage.getImageFor(urlString: data.bannerURL ?? "", completion: { (image, error) in
                if let image = image {
                    DispatchQueue.main.async {
                        self.bg.image = image
                    }
                }
            })
        }
    }
    
    
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
    
        var myString:String = "digio Cash"
        let myMutableString = NSMutableAttributedString(string: myString, attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 27, weight: .bold)])
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location:6,length:4))
        label.attributedText = myMutableString
        
        return label
    }()
    
    fileprivate let bg: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 12
        
        return iv
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        createView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createView() {
        self.addSubview(bg)
        self.addSubview(titleLabel)

        setupConstraints()
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            titleLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 10),
            
            bg.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 12),
            bg.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            bg.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            bg.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
        ])
    }
}
