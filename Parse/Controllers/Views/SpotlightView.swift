//
//  SpotlightView.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//


import UIKit

class SpotlightView: UIView {
    fileprivate var dataSource = [SpotlightVO]()
    
    var data: [SpotlightVO]? {
        didSet {
            guard let data = data else { return }
            self.dataSource = data
        }
    }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Olá, Maria"
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        
        return label
    }()
    
    let collectionViewSpotlight: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        let cvStoptlight = UICollectionView(frame: .zero, collectionViewLayout:  layout)
        cvStoptlight.translatesAutoresizingMaskIntoConstraints = false
        cvStoptlight.register(SpotlightCell.self, forCellWithReuseIdentifier: "spotlightCell")
        
        return cvStoptlight
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        createView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createView() {
        self.addSubview(collectionViewSpotlight)
        collectionViewSpotlight.backgroundColor = .white
        collectionViewSpotlight.delegate = self
        collectionViewSpotlight.dataSource = self
        
        self.addSubview(titleLabel)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            titleLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 10),
            
            collectionViewSpotlight.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 16),
            collectionViewSpotlight.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            collectionViewSpotlight.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            collectionViewSpotlight.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
        ])
    }
}

extension SpotlightView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout UICollectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 1.075, height: collectionView.frame.width/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "spotlightCell", for: indexPath) as! SpotlightCell
        cell.data = dataSource[indexPath.row]
        return cell
    }
}
