//
//  ProductView.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

class ProductView: UIView {
    fileprivate var dataSource = [ProductVO]()
    
    var data: [ProductVO]? {
        didSet {
            guard let data = data else { return }
            self.dataSource = data
        }
    }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Produtos"
        label.font = UIFont.systemFont(ofSize: 27, weight: .semibold)
        
        return label
    }()
    
    let collectionViewProduct: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        let cvProduct = UICollectionView(frame: .zero, collectionViewLayout:  layout)
        cvProduct.translatesAutoresizingMaskIntoConstraints = false
        cvProduct.register(ProductCell.self, forCellWithReuseIdentifier: "productCell")
        
        return cvProduct
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        createView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createView() {
        self.addSubview(collectionViewProduct)
        collectionViewProduct.backgroundColor = .white
        collectionViewProduct.delegate = self
        collectionViewProduct.dataSource = self
        
        self.addSubview(titleLabel)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            titleLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 10),
    
            collectionViewProduct.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 16),
            collectionViewProduct.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            collectionViewProduct.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            collectionViewProduct.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
        ])
    }
}

extension ProductView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout UICollectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3, height: collectionView.frame.width/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as! ProductCell
        cell.data = self.dataSource[indexPath.row]
        return cell
    }
}
