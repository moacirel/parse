//
//  Cash.swift
//  Parse
//
//  Created by Moacir Lamego on 27/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import Foundation

public struct Cash: Codable {
    var title: String?
    var bannerURL: String?
    var description: String?
}
