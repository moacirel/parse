//
//  Spotlight.swift
//  Parse
//
//  Created by Moacir Lamego on 27/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public struct Spotlight: Codable {
    var name: String?
    var bannerURL: String?
    var description: String?    
}
