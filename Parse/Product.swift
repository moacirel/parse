//
//  File.swift
//  Parse
//
//  Created by Moacir Lamego on 27/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import Foundation

public struct Product: Codable{
    var name: String?
    var imageURL: String?
    var description: String?
}
