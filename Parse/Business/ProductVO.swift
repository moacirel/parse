//
//  ProductVO.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public struct ProductVO {
    var name: String?
    var imageURL: String?
    var description: String?
}

