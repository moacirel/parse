//
//  AmazonAwsParser.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public class AmazonAwsParser {
    
    func parseToVo(amazonAws: AmazonAws) -> AmazonAwsVO {
        return ModelToVO(amazonAws: amazonAws)
    }
    
    func ModelToVO(amazonAws: AmazonAws) -> AmazonAwsVO {
        let spotlightVO = SpotlightParser().parseToVo(spotLights: amazonAws.spotlight ?? [Spotlight]())
        let productsVO = ProductParser().parseToVo(products: amazonAws.products ?? [Product]())
        let cashVO = CashParser().parseToVo(cash: amazonAws.cash ?? Cash())
        
        let vo = AmazonAwsVO(spotlightVO: spotlightVO, productsVO: productsVO, cashVO: cashVO)
        
        return vo
    }
}
