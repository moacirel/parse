//
//  ProductParser.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public class ProductParser {
    
    func parseToVo(products: [Product]) -> [ProductVO] {
        var vos = [ProductVO]()
        
        for produto in products {
            vos.append(ModelToVO(product: produto))
        }
        
        return vos
    }
    
    func ModelToVO(product: Product) -> ProductVO {
        let vo = ProductVO(name: product.name, imageURL: product.imageURL, description: product.description)
        
        return vo
    }
}

