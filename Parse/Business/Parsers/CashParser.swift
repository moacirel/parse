//
//  CashParser.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public class CashParser {
    
    func parseToVo(cash: Cash) -> CashVO {
        return ModelToVO(cash: cash)
    }
    
    func ModelToVO(cash: Cash) -> CashVO {

        let vo = CashVO.init(title: cash.title, bannerURL: cash.bannerURL, description: cash.description)
        
        return vo
    }
}


