//
//  SpotlightParser.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public class SpotlightParser {
    
    func parseToVo(spotLights: [Spotlight]) -> [SpotlightVO] {
        var vos = [SpotlightVO]()
        
        for sp in spotLights {
            vos.append(ModelToVO(spotLight: sp))
        }
        
        return vos
    }
    
    func ModelToVO(spotLight: Spotlight) -> SpotlightVO {
        let vo = SpotlightVO(name: spotLight.name, bannerURL: spotLight.bannerURL, description: spotLight.description)
        
        return vo
    }
}
