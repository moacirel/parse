//
//  AmazonAwsService.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import Foundation
import UIKit
    
public class AmazonAwsService {
    
   class func getData(completion: @escaping ((AmazonAws?, Error?) -> Void)) {
        let session = URLSession.shared
        let url = BaseURL
        let dataDecode = JSONDecoder()
        
        let task = session.dataTask(with: url) {
            (data, response, error) in
                        
            if let data = data,
                let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) {
                
                do {
                    let aws = try dataDecode.decode(AmazonAws.self, from: data)
                    completion(aws, error)
                    return
                } catch {
                    debugPrint("Error")
                    completion(AmazonAws(), error)
                    return
                }
            } else {
                completion(AmazonAws(), error)
            }
        }
        task.resume()
    }
}
