//
//  DownloadImage.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public class DownloadImage {
    
    // MARK: Download Image
    class func getImageFor(urlString: String, completion: @escaping ((UIImage?, Error?) -> Void)) {
        let session = URLSession.shared
        let url = URL(string: urlString)!
        print(urlString)
        
        let task = session.dataTask(with: url) { data, response, error in
            if let data = data,
                let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) {
                
                let image = UIImage(data: data)
                completion(image, nil)
            } else {
                completion(UIImage(), (DownloadError.statusCodeInvalid))
            }
            
            debugPrint("Data: \(String(describing: data))")
            debugPrint("Response: \(String(describing: response))")
            debugPrint("Error: \(String(describing: error))")
            
            return
        }
        
        task.resume()
    }
}

enum DownloadError: Error {
    case statusCodeInvalid
}
