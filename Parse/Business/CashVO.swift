//
//  CashVO.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public struct CashVO {
    var title: String?
    var bannerURL: String?
    var description: String?
}

