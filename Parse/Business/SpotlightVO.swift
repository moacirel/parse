//
//  SpotlightVO.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public struct SpotlightVO {
    var name: String?
    var bannerURL: String?
    var description: String?
}
