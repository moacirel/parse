//
//  AmazonAwsVO.swift
//  Parse
//
//  Created by Moacir Lamego on 28/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import UIKit

public struct AmazonAwsVO {
    var spotlightVO: [SpotlightVO]?
    var productsVO: [ProductVO]?
    var cashVO: CashVO?
}

