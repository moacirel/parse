//
//  AmazonAws.swift
//  Parse
//
//  Created by Moacir Lamego on 27/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import Foundation

public struct AmazonAws: Codable {
    var spotlight: [Spotlight]?
    var products: [Product]?
    var cash: Cash?
}
