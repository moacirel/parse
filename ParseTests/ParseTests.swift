//
//  ParseTests.swift
//  ParseTests
//
//  Created by Moacir Lamego on 27/08/20.
//  Copyright © 2020 MobilesApp. All rights reserved.
//

import XCTest
@testable import Parse

class ParseTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        AmazonAwsService.getData { (aws) in
            if let aws = aws {
                DispatchQueue.main.async {
                    print("Result Test: \(aws)")
                }
            }
        }
    }
}


